import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  ScrollController scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    scrollController.addListener(() {});
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
              snap: true,
              floating: true,
              expandedHeight: 100,
              title: Image.network(
                  'https://i.ytimg.com/vi/Ic3ZdD5ko7k/maxresdefault.jpg')),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  color: index.isOdd ? Colors.deepOrangeAccent : Colors.red,
                  height: 100.0,
                  child: Text(
                    '$index',
                    style: TextStyle(fontSize: 40, color: Colors.white),
                  ),
                );
              },
              childCount: 20,
            ),
          ),
        ],
      ),
    );
  }
}
